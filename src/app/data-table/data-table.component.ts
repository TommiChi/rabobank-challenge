import { Component, OnInit } from '@angular/core';
import { CsvDataService } from '../csv-data.service';
import { UploadErrorsService } from '../upload-errors.service';

@Component({
  selector: 'app-data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss']
})
export class DataTableComponent implements OnInit {

  tableData: Object = {};
  uploadErrors: any = { message: '' };
  equal: Boolean = false;
  lessThan: Boolean = false;
  greaterThan: Boolean = false;
  filterValue: Number;

  constructor(private data: CsvDataService, private errors: UploadErrorsService) { }

  ngOnInit() {
    this.data.currentData.subscribe(data => this.tableData = data);
    this.errors.currentErrors.subscribe(data => this.uploadErrors = Object.assign({ message: '' }, this.uploadErrors, data));
  }

  toggleFilter(value: string) {
    this[value] = !this[value];
  }
  
  setFilterValue(value: Number) { 
    this.filterValue = Number(value);
  }
  

  getCells(row) {
    return Object.keys(row).map(cell => row[cell]);
  }

  displayMessage() {
    const { message } = this.uploadErrors;
    return message || 'Please upload a CSV';
  }

  filteredBody(body: Array<Object> = []) {
    return (body).filter((row) => {
      const issueCount = Number(row['Issue count']);

      const conditions = {
        noFilter: !this.filterValue,
        noCheckBoxes: !this.equal && !this.lessThan && !this.greaterThan,
        equal: this.equal && issueCount === this.filterValue,
        lessThan: this.lessThan && issueCount < this.filterValue,
        greaterThan: this.greaterThan && issueCount > this.filterValue
      };

      for (const key of Object.keys(conditions)) {
        if (conditions[key]) {
          return true;
        }
      }

      return false;
    })
  }

}
