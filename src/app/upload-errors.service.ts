import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class UploadErrorsService {
  private dataSource = new BehaviorSubject({});
  currentErrors = this.dataSource.asObservable();

  constructor() { }

  updateError(data: Object) {
    this.dataSource.next(data);
  }
}
