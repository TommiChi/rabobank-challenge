import { Component, OnInit } from '@angular/core';
import { CsvDataService } from '../csv-data.service';
import { UploadErrorsService } from '../upload-errors.service';

@Component({
  selector: 'app-uploader',
  templateUrl: './uploader.component.html',
  styleUrls: ['./uploader.component.scss']
})
export class UploaderComponent implements OnInit {

  constructor(private data: CsvDataService, private errors: UploadErrorsService) { }

  ngOnInit() {
  }

  readFile(file: Blob, action: string) {
    return new Promise((resolve, reject) => {
      const reader = new FileReader();

      reader.onload = () => {
        resolve(reader.result.toString());
      };

      reader.onerror = (error) => {
        reject(error);
      };

      reader[action](file);
    });
  }

  isValidType(file: Blob) {
    return this.readFile(file, 'readAsDataURL')
      .then((base64String: string) => {
        const fileType = base64String.split(';')[0].split(':')[1];

        return ['text/plain', 'text/csv'].indexOf(fileType) > -1 ?
          Promise.resolve(file) : Promise.reject(new Error('The file uploaded is not a valid CSV file'));
      });
  }

  newTable(data) {
    this.data.updateData(data);
  }

  csvToArray(data: String) {
    const dataArray = data.replace(/"/g, '').split(/\r/);

    const tableHeaders = dataArray.shift().split(',');
    const maxColumns = tableHeaders.length;

    const tableBody = dataArray.map((row) => {
      return row.split(',').reduce((rowObj, cell, index) => {
        if (index < maxColumns) {
          const updatedObject = { ...rowObj, ...{ [tableHeaders[index]]: cell.replace(/\r|\n/g, '') } };
          return updatedObject;
        }
        return rowObj;
      }, {});
    });

    return { tableHeaders, tableBody };
  }

  getCsvData(file: Blob) {
    this.isValidType(file)
      .then((validated: Blob) => this.readFile(validated, 'readAsText'))
      .then((csvData: String) => {
        this.errors.updateError({ message: '' });
        this.newTable(this.csvToArray(csvData));
      })
      .catch((error: Error) => {
        const { message } = error;
        this.errors.updateError({ message });
      });
  }
}
