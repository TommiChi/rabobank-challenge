import { TestBed } from '@angular/core/testing';

import { UploadErrorsService } from './upload-errors.service';

describe('UploadErrorsService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: UploadErrorsService = TestBed.get(UploadErrorsService);
    expect(service).toBeTruthy();
  });
});
