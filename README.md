# Rabobank Challenge

A small web app challenge that Rabobank will use for evaluation purposes

## Purpose of App

The purpose of this web application is to provide a user interface where end users can upload and manipulate CSV files.

## Challenge

The instructions I was provided for the challenge were:
- You should be able to select this file from my computer and see the results on screen
- You should be able to filter the results based on minimal issue count

## Assumptions

- No backend is required, that the uploaded CSV should be sent to
- Only greenfield browsers are supported (although in a real production environment, features from polyfills.js will be turned on)
- Unit tests are not a requirement
- Internationalisation is not a requirement (otherwise the i18n features built into Angular would have been used rather than hard-coded text)

## Observations

- The code was effectively done in 2 passes;
  1. As I had a pretty clear idea of what I wanted to do, the original functionality was essentially all conceived and pushed in one shot (first pass)
  2. I subsequently put on my "how would I test this" hat and made a few alterations, such as basic error handling (second pass).
- There are many ways to skin a cat (or to bind events). The event binding strategy I opted for is one of 3 possible ways that I can think of. The basis of my choice boils down to the following:
  1. The code currently in place is quite nicely self contained
  2. The code currently in place is easily reusable, and can also easily be migrated to other frameworks/libraries without much refactoring
- There are plugins in the wild such as ng2-table, that do a whole bunch of stuff with regards to sorting, filtering, etc. The functionality required for this demo was small enough to be easy to implement and maintain. I may have taking more time researching the right plugin and figuring out the API than writing the small piece of custom code currently in place.
- More complex error handling and message dispatching could be implemented, however my focus was on making the most basic and "transferrable" implmentation possible.
- There is a lot more that can be done with this component, however I limited my scope to the basic most important things that would have to make their way into this feature (a sort of MVP)

## Browser Support

For the purposes of this challenge, polyfills.ts has remained disabled. Therefore IE9 - 11 is likely not to be supported for the most part

## Running the code

- Checkout the repository
- Run npm install
- Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.